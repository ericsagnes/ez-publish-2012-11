<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=firststeps
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=eztags
ActiveExtensions[]=ezautosave
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=firststeps
SiteList[]
SiteList[]=firststeps
SiteList[]=front
SiteList[]=jpn
SiteList[]=back
RootNodeDepth=1

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=firststeps
AvailableSiteAccessList[]=front
AvailableSiteAccessList[]=jpn
AvailableSiteAccessList[]=back
MatchOrder=uri
HostMatchMapItems[]

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[jpn]=Jpn

[FileSettings]
VarDir=var/ezwebin_site

[MailSettings]
Transport=sendmail
AdminEmail=test@test.com
EmailSender=

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline

####################
# 開発設定
#####################

[ContentSettings]
ViewCaching=disabled

[DebugSettings]
DebugOutput=enabled

[TemplateSettings]
Debug=enabled
ShowXHTMLCode=disabled
TemplateCompile=disabled
TemplateCache=disabled
ShowUsedTemplates=enabled

[OverrideSettings]
Cache=disabled


*/ ?>
