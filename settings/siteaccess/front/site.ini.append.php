<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=root
Password=root
Database=ez_session_2012_11
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Website Interface (without demo content)
SiteURL=ezsession-2012-11.arch
LoginPage=embedded
AdditionalLoginFormActionURL=http://ezsession-2012-11.arch/back/user/login

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=front
RelatedSiteAccessList[]=jpn
RelatedSiteAccessList[]=back
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=ezwebin
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=jpn-JP
ContentObjectLocale=jpn-JP
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=jpn-JP
TextTranslation=enabled

[FileSettings]
VarDir=var/ezwebin_site_clean

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=test@test.com
EmailSender=
*/ ?>