#eZ Publish勉強会2012年11月

##課題

eZ Publishをインストールして、エクステンションを作り、ベストプラクティスにそった開発環境を設定する。

---

##ステップ0 - インストール

eZ Publishのインストール:

###1.ダウンロード

    wget http://share.ez.no/content/download/130661/708336/version/2/file/ezpublish_community_project-2012.8-with_ezc.tar.gz

    tar -xvzf ezpublish_community_project-2012.8-with_ezc.tar.gz

    mv ezpublish_community_project-2012.8-with_ezc ez_firststeps

###2.データベースを作成

    mysql -uユーザ -p  
    create database ez_firststeps character utf8;  
    exit

ブラウザからアクセスし、インストールを完了させます。

---

##ステップ1 - エクステンションの作成

###1.エクステンションフォルダーの作成

    mkdir -p extension/firststeps/

###2.エクステンションを有効にする  
settings/override/site.ini.append.php の

    [ExtensionSettings]  
    ActiveExtensions[]  
    ActiveExtensions[]=ezmultiupload  
を下記のように変更する

    [ExtensionSettings]  
    ActiveExtensions[]  
    ActiveExtensions[]=firststeps  
    ActiveExtensions[]=ezmultiupload  

管理画面の「システム設定 > エクステンション」でfirststepsが有効であるのを確認。

---

##ステップ2 - サイトアクセス

ベストプラクティスとして、できるだけエクステンションで完結させたいので、
サイトアクセスをfirststepsエクステンションに作る

###1.サイトアクセスの作成

まずはサイトアクセスのフォルダーを作ります

    mkdir -p extension/firststeps/settings/siteaccess/firststeps

次は標準の設定ファイルをサイトアクセスにコピーします(frontはインストールの時に設定したサイトアクセス)

    cp settings/siteaccess/front/* extension/firststeps/settings/siteaccess/firststeps

ゴミファイルを削除する

    rm extension/firststeps/settings/siteaccess/firststeps/*~

###2.サイトアクセスの登録

settings/override/site.ini.append.phpに

    [SiteSettings]
    DefaultAccess=jpn
    SiteList[]
    SiteList[]=front
    SiteList[]=jpn
    SiteList[]=back

を下記のように変更します

    [SiteSettings]
    DefaultAccess=firststeps
    SiteList[]
    SiteList[]=firststeps
    SiteList[]=front
    SiteList[]=jpn
    SiteList[]=back

キャッシュを削除し:

    php bin/php/ezcache.php --clear-all

ページをリロードします。

「アクセスできません」は表示されます、原因は匿名ユーザは「firststeps」サイトアクセス
を閲覧する権限がありません。

###3.権限の設定

管理画面の「ユーザ管理 > ロールとポリシー」にクリックし、Anonymousをクリックします。

権限の中に

    user 	login 	SiteAccess( front )  
    user 	login 	SiteAccess( jpn )

があります。どちらかを編集し、「firststeps」サイトアクセスを追加し、ロールを保存します。

---

##ステップ3 - デザイン

###1.eZ Publishに開発設定を適用する

デザイン設定に向けて、キャッシュを無効にし、デバッグを有効にします。

settings/override/site.ini.append.phpの終わりに下記のブロックを追加:

    ####################
    # 開発設定
    ####################

    [ContentSettings]
    ViewCaching=disabled

    [DebugSettings]
    DebugOutput=enabled

    [TemplateSettings]
    Debug=enabled
    ShowXHTMLCode=disabled
    TemplateCompile=disabled
    TemplateCache=disabled
    ShowUsedTemplates=enabled

    [OverrideSettings]
    Cache=disabled

キャッシュを削除し:

    php bin/php/ezcache.php --clear-all

ページをリロードすると、ページの下にデバッグ情報が表示されます。

###2.デザインエクステンションの登録

firststepsエクステンションをデザインエクステンションとして登録:

extension/firststeps/settings に design.ini.append.php を作成し、下記を追加します。

    <?php /* #?ini charset="utf-8"?

    [ExtensionSettings]
    DesignExtensions[]=firststeps

    */ ?>

###3.フォルダーの準備

デザインフォルダーの追加:

    mkdir -p extension/firststeps/design/firstdesign/

デザインに必要なフォルダーを作ります:

- images: コンテンツでない画像
- stylesheets: cssファイル
- javascript: jsファイル
- templates: テンプレートファイル
- override: テンプレートオーバーライド

下記のコマンドで作ります:

    mkdir extension/firststeps/design/firstdesign/{images,override,stylesheets,templates,javascript}

###4.フォルダーの準備 - ベストプラクティス (任意)

オーバーライドを管理しやすくするために下記のように作ります

extension/firststeps/design/firstdesign/override/templates

- page ページ別のオーバーライド（node_id等）
- class クラス別のオーバーライド

下記のコマンドで作ります:

    mkdir -p extension/firststeps/design/firstdesign/override/templates/{page,class}

この構造の例:

    - page/
      - company.tpl
      - company/
        - about.tpl
    - class/
      - folder/
        - full.tpl
        - line.tpl
        - section/
          - sports/
            - full.tpl

標準テンプレートフォルダーは

- parts ヘッダー、フッターなどのincludeする各テンプレートパーツ
- node/view デフォルトのビューモード

下記のコマンドで作ります:

    mkdir -p extension/firststeps/design/firstdesign/templates/{node/view,parts}

この構造の例:

    - parts/
      - header.tpl
      - footer.tpl
    - node/
      - view/
        - full.tpl
        - line.tpl

###5.サイトアクセスにデザインを追加する

extension/firststeps/settings/siteaccess/firststeps/site.ini.append.phpの[DesignSettings]ブロックを下記から

    [DesignSettings]
    SiteDesign=ezwebin
    AdditionalSiteDesignList[]
    AdditionalSiteDesignList[]=base

下記に変更します

    [DesignSettings]
    SiteDesign=firstdesign
    AdditionalSiteDesignList[]
    AdditionalSiteDesignList[]=ezwebin
    AdditionalSiteDesignList[]=base

---

##ステップ4 - テンプレートの追加

###1.レイアウトのカスタマイズ - pagelayout.tpl

標準オーバーライドであるため、かきのフォルダーに作ります。

    extension/firststeps/design/firstdesign/templates/pagelayout.tpl

pagelayout.tplで重要な変数は: 

- $module_result.content ビューの結果
- ezpagedata() 現ページのいろんなデータ（ezwebin必須）

スクラッチで書くのも良いが、慣れてない間はezwebin等のテンプレートをコピーし、
編集するのはお勧め。

ナビゲーション、ヘッダー、フッターなどはパーツ化して、{include}で呼び出す。

ナビゲーションの場合:

    {include uri='design:parts/navigation.tpl'}

テンプレートは下記のように置きます

    extension/firststeps/design/firstdesign/templates/parts/navigation.tpl

デザインにあるcssとjsは{ezdesign}を使って読み込みます:

    <link rel="stylesheet" href={'stylesheets/style.css'|ezdesign} type="text/css" media="screen" />  
    <script charset="utf-8" src={'javascript/script.js'|ezdesign} type="text/javascript"></script>


###2.ノードテンプレート

管理画面を使って「folder」クラスのオブジェクトを作ります。

folderのデフォルト表示(full)のテンプレートを作ります。

テンプレートが利用されるためにオーバーライド設定を追加します。

    extension/firststeps/settings/siteaccess/firststeps/override.ini.append.php

の上に下記のオーバーライドブロックを追加する

    [folder_full]
    Source=node/view/full.tpl
    MatchFile=class/folder/full.tpl
    Subdir=templates
    Match[class_identifier]=folder

テンプレートを下記のパスに置きます。

    extension/firststeps/design/firstdesign/override/templates/class/folder/full.tpl

ノードテンプレートでは**$node**変数が利用できます。  
**$node**は現在のノードの情報を細かく持っています:

- $node.name ノードの名前
- $node.url_alias ノードのURL
- $node.data_map ノードの属性

ノードテンプレートにノードの属性を表示するには attribute_view_gui が使えます。例：

    {attribute_view_gui attribute=$node.data_map.description}

他のノードもフェッチできます（全テンプレート共通）。例:

    {def $items = fetch('content','list', hash(
        'parent_node_id', 2,
        'limit', 5
    ))}

---

##ステップ5 - 完成に向けて - ベストプラクティス

管理画面で必要なコンテンツクラスを作って、それらのテンプレートを追加する。

gitで各エクステンションを管理して、メインエクステンションにdataと言うフォルダーを作り、そこに定期的にデータベースのダンプをとり、コミットする（開発段階）:

    mysqldump -uユーザ名 -p --add-drop-table データベース名 > extension/firststeps/data/db.sql

必要に応じて、エクステンションに新しいテンプレートオペーレーター、モジュール、ワークフローイベントなどを追加する。

/kernel、/lib、/design等のデフォルトフォルダーはできるだけ変更しないで、エクステンションで完結させる。

メインエクステンションで再利用で汎用な機能やテンプレートは別なエクステンションにする。

{node_view_gui}、{attribute_view_gui}と{include}を活用して、2回同じテンプレートロジックを書かないようにします。

複雑なテンプレートロジックはテンプレートオペーレーターでまとめましょう。

変なエラーが出れば、まずはキャッシュを削除する:

    php bin/php/ezcache.php --clear-all

[リファレンス](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/)と[API](http://pubsvn.ez.no/doxygen/trunk/html/)を活用する。




















